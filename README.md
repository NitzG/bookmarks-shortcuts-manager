# Bookmarks Shortcuts Manager

This is a chrome extension that let you access your bookmarks through **keyboard shortcuts**!

I created it because bookmarks are great and keyboard shortcuts are great so together they must be super doper awesome!

## Installation

`TODO`: When I will know how to install it I will fill this part

## Usage

Use `Alt` + The number of the bookmarks you want to open it in the current tab.

## Contributing

You are welcomed to join me :)
I am not a real JS programmer so feel free to improve my code.

## How it works?

At first I thought about using a background script and [chrome extension commands](https://developer.chrome.com/extensions/commands).
But it turns out that you are limited for only `4` commands per extension (I have no clue why...).
I implemented a simple extension using this idea in commit `afede98`.

**_hf using this :)_**
