"use strict";

var BOOKMARKS_BAR_ID = "1";

function compareByIndex(bookmark1, bookmark2) {
  return bookmark1.index - bookmark1.index;
}

var ALT_LENGTH = 3;

chrome.commands.onCommand.addListener(function(command) {
  var bookmarkIndex = parseInt(command.substring(ALT_LENGTH)) - 1;

  chrome.bookmarks.getChildren(BOOKMARKS_BAR_ID, function(bookmarks) {
    bookmarks.sort(compareByIndex);

    if (bookmarks.length < bookmarkIndex) {
      return;
    }

    var bookmarkOrFolder = bookmarks[bookmarkIndex];
    if (bookmarkOrFolder.url) {
      var bookmark = bookmarkOrFolder; // bookmark
      chrome.tabs.query({ currentWindow: true, active: true }, function(tab) {
        chrome.tabs.update(tab.id, { url: bookmark.url }, function(tab) {
          console.log("Changed tab", tab.id, "to the url:", bookmark.url);
        });
      });
    } else {
      var folder = bookmarkOrFolder; // folder
      alert("No support for folders just yet...");
      console.log("Someone tried to open folder:", folder.title);
    }
  });
});
